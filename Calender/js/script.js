// =================|Decalaration|=================
var theCalender = jsCalendar.new("#mainCalender");
var locations = document.getElementById("location");
var desc = document.getElementById("desc");

var cDate = "";
var selectedDates = [];
var datas = [];

function checkSelected(dataDate) {
  return dataDate == cDate;
}

// =================|On load|=================
$(document).ready(() => {
  selectAll();
});

function selectAll() {
  selectedDates = JSON.parse(localStorage.getItem("selectedDate"));
  datas = JSON.parse(localStorage.getItem("data"));
  theCalender.select(selectedDates);
}

// =================|cols="30" rows="10"On select|=================
theCalender.onDateClick((event, date) => {
  var clickedDate = date.toString();
  var dateToArray = clickedDate.split(" ");
  if (dateToArray[1] === "Jan") {
    cDate = dateToArray[2] + "/1/" + dateToArray[3];
  } else if (dateToArray[1] === "Feb") {
    cDate = dateToArray[2] + "/2/" + dateToArray[3];
  } else if (dateToArray[1] === "Mar") {
    cDate = dateToArray[2] + "/3/" + "/" + dateToArray[3];
  } else if (dateToArray[1] === "Apr") {
    cDate = dateToArray[2] + "/4/" + dateToArray[3];
  } else if (dateToArray[1] === "Mei") {
    cDate = dateToArray[2] + "/5/" + dateToArray[3];
  } else if (dateToArray[1] === "Jun") {
    cDate = dateToArray[2] + "/6/" + dateToArray[3];
  } else if (dateToArray[1] === "Jul") {
    cDate = dateToArray[2] + "/7/" + dateToArray[3];
  } else if (dateToArray[1] === "Aug") {
    cDate = dateToArray[2] + "/8/" + dateToArray[3];
  } else if (dateToArray[1] === "Sep") {
    cDate = dateToArray[2] + "/9/" + dateToArray[3];
  } else if (dateToArray[1] === "Oct") {
    cDate = dateToArray[2] + "/10/" + dateToArray[3];
  } else if (dateToArray[1] === "Nov") {
    cDate = dateToArray[2] + "/11/" + dateToArray[3];
  } else if (dateToArray[1] === "Dec") {
    cDate = dateToArray[2] + "/12/" + dateToArray[3];
  }

  if (cDate == selectedDates.find(checkSelected) && datas != null) {
    var selectedData = datas.find(obj => obj.dataDate == cDate);
    locations.value = selectedData.dataLocation;
    desc.value = selectedData.dataDesc;

    $("#target").innerHTML = cDate.toString();
  } else {
    locations.value = "";
    desc.value = "";
    $("#target").innerHTML = cDate.toString();
  }
});

$("#save").on("click", () => {
  var toSaveData = {
    dataDate: cDate,
    dataLocation: locations.value,
    dataDesc: desc.value
  };

  if (datas == null) {
    datas = [toSaveData];
    selectedDates = [cDate];

    localStorage.setItem("data", JSON.stringify(datas));
    localStorage.setItem("selectedDate", JSON.stringify(selectedDates));

    selectAll();
  } else if (locations.value == "" || desc.value == "") {
    datas.splice(selectedDates.indexOf(cDate), 1);
    selectedDates.splice(selectedDates.indexOf(cDate), 1);

    localStorage.setItem("data", JSON.stringify(datas));
    localStorage.setItem("selectedDate", JSON.stringify(selectedDates));

    theCalender.unselect(cDate);
  } else {
    datas.push(toSaveData);
    selectedDates.push(cDate);

    localStorage.setItem("data", JSON.stringify(datas));
    localStorage.setItem("selectedDate", JSON.stringify(selectedDates));
    selectAll();
  }
});
